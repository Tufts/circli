CIRCLI
======
This is the repository for the CIRCLI server that acts
as the backend to the circli-app project.

Install
-------
~~~~{.sh}
git clone https://Tufts@bitbucket.org/Tufts/circli.git
virtualenv env
source env/bin/activate
export PATH=$PATH:/usr/local/mysql/bin
pip install -r requirements.txt
~~~~
Run
---
~~~~{.sh}
python manage.py runserver
~~~~
