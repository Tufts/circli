from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from login.views import logout_page, register, register_success, api_auth
from partypic.views import (
        home, 
        add_group, 
        create_group, 
        group_page, 
        upload_photo, 
        change_group_title,
        post_comment,
        delete_object,
)
from partypic.api import (
        new_group_hash,
        CreateGroup,
        CheckGroupExists,
        CheckUserExists,
        CreateUser,
        ValidateUser,
        RequestPasswordReset,
        ResetPassword,
        GetGroupContent, 
        ListGroupsByUser, 
        RequestRelationship, 
        SearchUsers, 
        GetRelationships,
        GetUserDetails,
        UploadImage,
        UploadAvatar,
        AcceptRelationship,
        ResendConfirmationEmail,
        LikePhoto,
        InviteToGroup,
        SendMessage,
        RegisterDevice,
        UpdateGroupName,
        LeaveGroup,
        GetObject,
)

admin.autodiscover()

urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'codevitae.views.home', name='home'),
        # url(r'^codevitae/', include('codevitae.foo.urls')),

        # Uncomment the admin/doc line below to enable admin documentation:
        # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

        # Uncomment the next line to enable the admin:
        url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
        url(r'^admin/', include(admin.site.urls)),
        url('^', include('django.contrib.auth.urls')),
        url(r'^$', 'django.contrib.auth.views.login'),
        url(r'^logout/$', logout_page, {'next_page': '/'}),
        url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
        url(r'^register/$', register),
        url(r'^register/success/$', register_success),
        url(r'^home/$', home),
        url(r'^add-group/$', add_group),
        url(r'^create-new-group/$', create_group),
        url(r'^group-not-found/$', home),
        url(r'^group/(?P<group_hash>.{5})/$', group_page),
        url(r'^group/(?P<group_hash>.{5})/upload/$', upload_photo),
        url(r'^change-group-title/$', change_group_title),
        url(r'^post-comment/$', post_comment),
        url(r'^delete-(?P<type>comment|photo)/$', delete_object),
        url(r'^api/auth/$', api_auth),
        url(r'^api/lookup/$', new_group_hash),
        url(r'^api/create-group/$', CreateGroup.as_view()),
        url(r'^api/check-group-exists/$', CheckGroupExists.as_view()),
        url(r'^api/check-user-exists/$', CheckUserExists),
        url(r'api/create-user/$', CreateUser.as_view(), name="createUser"),
        url(r'api/validate-user/$', ValidateUser.as_view(), name="validateUser"),
        url(r'^api/forgot-password/$', RequestPasswordReset.as_view()),
        url(r'^api/reset-password/$', ResetPassword.as_view()),
        url(r'^api/group-content/$', GetGroupContent.as_view()),
        url(r'^api/list/$', ListGroupsByUser.as_view()),
        url(r'^api/friend-request/$', RequestRelationship.as_view()),
        # TODO
        #url(r'^api/delete-request/$', DeleteRelationshipRequest.as_view()),
        url(r'^api/accept-friend/$', AcceptRelationship.as_view()),
        url(r'^api/search/$', SearchUsers.as_view()),
        url(r'^api/list-friends/$', GetRelationships.as_view()),
        url(r'^api/user/$', GetUserDetails.as_view()),
        url(r'^api/upload-image/$', UploadImage.as_view()),
        url(r'^api/upload-avatar/$', UploadAvatar.as_view()),
        url(r'^api/resend-email/$', ResendConfirmationEmail.as_view()),
        url(r'^api/like-photo/$', LikePhoto.as_view()),
        url(r'^api/invite-to-group/$', InviteToGroup.as_view()),
        url(r'^api/send-message/$', SendMessage.as_view()),
        url(r'^api/register-device/$', RegisterDevice.as_view()),
        url(r'^api/update-group-name/$', UpdateGroupName.as_view()),
        url(r'^api/leave-group/$', LeaveGroup.as_view()),
        url(r'^api/get-object/$', GetObject.as_view()),
       #url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, }),
)

