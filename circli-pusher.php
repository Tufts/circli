<?php
// Testing julie account
// Put your device token here (without spaces):
//$deviceToken = 'ddbf89c221 6e432fb121 903de336b9 f3b3418562 49e743e1df 6207b364ef 91fb';

// Put your private key's passphrase here:
$passphrase = '';

$deviceToken = $argv[1];
$message = $argv[2];
$view = $argv[3];
$action = $argv[4];
$params = explode(",", $argv[5]);

# Get the mode, "sandbox" or "production", from the environment.
$mode = getenv('CIRCLIMODE');

$parameters = array();
foreach ($params as $parameter) {
    $aux = explode("=", $parameter);
    $parameters[$aux[0]] = $aux[1];
}

// PARAMETERS AND ACTION are optional arguments.
if (!$deviceToken || !$message || !$view) {
    echo "\nCheck your notification parameters!";
    echo "\nDevice Token: " . $deviceToken;
    echo "\nMessage: " . $message;
    echo "\nView: " . $view;

    exit('\nExample Usage: $php cicli-pusher.php \'<token>\' \'<message>\' \'<view>\' \'<action>\'' . "\n");

}

////////////////////////////////////////////////////////////////////////////////s
$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'Circli.Push.Certificate.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS serverss
//Sandbox
if ($mode == 'sandbox') {
  $fp = stream_socket_client(
    'ssl://gateway.sandbox.push.apple.com:2195', $err,
    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
}
// Production
else if ($mode == 'production') {
  $fp = stream_socket_client(
    'ssl://gateway.push.apple.com:2195', $err,
    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
}
// None of the above.
else {
  echo "Environment variable, CIRCLIMODE, was not set to either sandbox or production\n";
}

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
  'alert' => $message,
  'sound' => 'default',
  'badge' => 1,
  'view' => $view,
  'action' => $action,
  'params' => $parameters,
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else {
  echo 'Message successfully delivered' . PHP_EOL;
  var_dump($body);
}

// Close the connection to the server
fclose($fp);
