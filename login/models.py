from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Authorization(models.Model):
    hash = models.CharField(max_length=20, primary_key=True)
    salt = models.CharField(max_length=5)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
