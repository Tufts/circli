import re
import traceback
from login.functions import crop_image_from_form
from partypic.hasher import new_hash, user_hash

from login.forms import *
from login.models import Authorization
from partypic.models import Profile, Photo
from django.contrib.auth.decorators  import login_required
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, JsonResponse
from django.template import RequestContext
from django.contrib.auth import authenticate

@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                user = User.objects.create_user(
                        username = form.cleaned_data['username'],
                        password = form.cleaned_data['password1'],
                        email = form.cleaned_data['email'],
                )
                avatar = request.FILES['avatar']
                image = crop_image_from_form(avatar, form)
                profile = Profile.objects.create(user=user)
                photo = Photo.objects.create(owner=profile, image=image)
                photo.image.name = re.sub("partypic/static/", "", photo.image.name)
                profile.avatar = photo

                profile.save()
                photo.save()

                return HttpResponseRedirect('/register/success/')
            except Exception as e:
                traceback.print_exc()
                print("Found an error: %s" % str(e))
                return HttpResponseRedirect('/register/')
        
        # Need to program an ELSE here. People might click register by accident!
        else :
            return HttpResponseRedirect('/register/')
    
    else: # request.method == 'GET'
        form = RegistrationForm()
        variables = RequestContext(request, {'form': form})
        return render_to_response('registration/register.html', variables)

def register_success(request):
    return render_to_response('registration/success.html')

def logout_page(request):
    logout(request)

@login_required
def home(request):
    return render_to_response('home.html', {'user': request.user})

@csrf_protect
def api_auth(request):
    json_response = None
    if request.method == 'POST':
        form = request.POST
        try:
            username = form['user']
            password = form['password']
            user = authenticate(username=form['user'], password=form['password'])
            
            if user: # Exists
                salt = new_hash()
                user_hash = user_hash("%s%s%s" % (salt, username, password))
                auth = Authorization(hash=user_hash, user=user, salt=salt)

                json_response = {'auth': user_hash}
            else:
                json_response = {'error': "Wrong username or password. Try again."}

        except Exception as e:
            print("Found an error: %s\n" %  str(e))
            json_response = {'error': "A fatal error occured in the server. Please try again later."}
    
    return JsonResponse(json_response)