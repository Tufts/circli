// Enable jCrop widget:
function enable_jcrop() {
    jQuery(function($) {
        $("#preview").Jcrop(
            { trackDocument: true,
              aspectRatio: 1,
              minSize: [50,50],
              setSelect: [0,0,50,50],
              onSelect: updateCoords
            }    
        );
    });
}
// End of Enable jCrop widget

// Update cropped image coordinates:
function updateCoords(c) {
    $('#id_c_x1').val(c.x);
    $('#id_c_y1').val(c.y);
    $('#id_c_x2').val(c.x2);
    $('#id_c_y2').val(c.y2);
    $('#id_c_w').val($('#preview').width());
    $('#id_c_h').val($('#preview').height());
}
// End update cropped image coordinates.

// Profile pic preview function:
// When user selects an image file, use the result as src for preview img tag.
$('#id_avatar').on('change', function() {
    var reader = new FileReader();

    reader.onload = function(e) {
        document.getElementById('preview').src = e.target.result;
        enable_jcrop();
    } 

    reader.readAsDataURL(this.files[0]);
});
// End of Profile pic preview function.
