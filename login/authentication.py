from rest_framework.authentication import BasicAuthentication
from django.urls import reverse

class AnonymousPostAuthentication(BasicAuthentication):
    """ No auth on post / for user creation """

    def is_authenticated(self, request, **kwargs):
        """ If PATH and POST match your scenario, 
        don't check auth, otherwise fall back to parent """

        createurl = reverse('createUser')

        if request.path == createurl and request.method == "POST" :
            return True
        else:
            return (super(AnonymousPostAuthentication,self)
                        .is_authenticated(request, **kwargs))