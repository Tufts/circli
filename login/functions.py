from PIL import Image
import io
from os.path import basename

from django.core.files.base import ContentFile

def crop_image_from_form(image, form):
    '''
    This function receives an in-memory image file usually received from the request.FILES variable,
    and crops it to a new size determined the form data. Note that form has to contain the variables
    c_x1, c_y1, c_x2, c_y2, c_w and c_h.
    '''
    x1 = form.cleaned_data['c_x1']
    y1  = form.cleaned_data['c_y1']
    x2 = form.cleaned_data['c_x2']
    y2 = form.cleaned_data['c_y2']
    w = form.cleaned_data['c_w']
    h = form.cleaned_data['c_h']

    original = Image.open(image)
    (W,H) = original.size

    # Following transformation is necessary.
    # x1 - X1
    # w - W
    # X1 = (W/w)*x1
    #
    # Same for X2, Y1, Y2
    
    X1 = (W/w)*x1
    Y1 = (H/h)*y1
    X2 = (W/w)*x2
    Y2 = (H/h)*y2

    # Image is cropped using the function below.
    cropped = original.crop((X1,Y1,X2,Y2))

    # Cropping and saving to file doesn't cut it though. Django expects
    # the uploaded image to be in memory, not file. Need BytesIO to finish.
    # ContentFile creates the expected object type.
    cropped_io = io.BytesIO()
    cropped.save(cropped_io, format='PNG')
    content = ContentFile(cropped_io.getvalue(), basename(image._name))
    return content
