from hashlib import sha1 as sha
import random

def new_hash():
    i = int(10000000*random.random())
    s = sha(str(i).encode('UTF-8')).hexdigest()
    s_hash = s[0] + s[i % 40] + s[2*i % 40] + s[3*i % 40] + s[4*i % 40]
    return s_hash.upper()

def user_hash(salted_user_password):
    i = int(10000000*random.random())
    s = sha(salted_user_password.encode('UTF-8')).hexdigest()
    return s[0:19].upper()
