from functools import wraps

class KeyDict(dict):
    '''
    KeyDict is a dictionary that only allows string keywords (non-numeric).
    '''

    def __add__(self, x):
        return KeyDict(self, **x)

def use_socket(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        kwargs['data'] = KeyDict({'socketed': True})
        result = func(*args, **kwargs)
        return result
    return wrapper