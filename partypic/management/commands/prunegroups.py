from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from partypic.models import Group

from datetime import timedelta

class Command(BaseCommand):
    help = 'Deletes all groups that are older than the specified delay.'

    def add_arguments(self, parser):
        parser.add_argument('delay', nargs='+', type=int)

    def handle(self, *args, **options):
        now = timezone.now()
        delay = options['delay'][0]

        query = Group.objects.filter(creation_date__lte=now-timedelta(days=delay))
        for group in query:
            self.stdout.write('Deleted group %s : "%s"' % (group.group_hash,group.group_title))
            group.delete()