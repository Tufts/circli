from .serializable import ModelSerializer
from .models import Message, Photo
from rest_framework import serializers


class MessageSerializer(ModelSerializer):
    owner = serializers.StringRelatedField()
    group = serializers.StringRelatedField()

    class Meta:
        otype = 'message'
        model = Message
        fields = ['id', 'owner', 'group', 'photo', 'publish_date', 'content']
        
class PhotoSerializer(ModelSerializer):
    url = serializers.CharField(source='imagepath')
    owner = serializers.StringRelatedField()
    #reactions = serializers.StringRelatedField(many=True)
    reactions = serializers.IntegerField(source='reactions.count', read_only=True)

    class Meta:
        otype = 'photo'
        model = Photo
        fields = ['id', 'owner', 'url', 'caption', 'submission_date','reactions']