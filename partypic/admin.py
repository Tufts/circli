from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Group)
admin.site.register(Photo)
admin.site.register(Profile)
admin.site.register(Device)
admin.site.register(Message)
admin.site.register(Reaction)
admin.site.register(Avatar)
admin.site.register(Relationship)