from datetime import datetime
from partypic.hasher import new_hash
from partypic.decorators import KeyDict, use_socket
import re 

from django.contrib.auth.decorators  import login_required
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect, JsonResponse
from django.template import RequestContext
from django.contrib.staticfiles.templatetags.staticfiles import static
from partypic.models import Group, Photo, Message, Profile, Reaction

@login_required
@csrf_protect
@use_socket
def home(request, data=None):
    profile = Profile.objects.get(user=request.user)
    groups = Group.objects.all().filter(members=profile)
    data = data + KeyDict({'profile': profile, 'groups': groups})
    context = RequestContext(request, data)
    return render_to_response('home.html', context)
    
@login_required
@csrf_protect
def add_group(request):
    if request.method == 'POST':
        form = request.POST
        if form['addGroup']:
            user = request.user
            profile = Profile.objects.get(user=user)
            group_hash = form['addGroup']
            try:
                g = Group.objects.get(group_hash=group_hash)
                g.members.add(profile)
                g.save()
            except:
                print("Could not find group!")
                return HttpResponseRedirect('/group-not-found')
    return HttpResponseRedirect('/home')

@login_required
@csrf_protect
def create_group(request):
    if request.method == 'GET':
        user = request.user
        profile = Profile.objects.get(user=user)
        not_done = True
        while not_done:
            group_hash = new_hash()
            try:
                g = Group.objects.create(group_hash=group_hash, creator=profile, creation_date = datetime.utcnow())
                g.members.add(profile)
                g.save()
                not_done = False
            except:
                print("Group already exists. Trying again.")
                pass
        return HttpResponseRedirect('/home')
    return HttpResponseRedirect('/home')

@login_required
@csrf_protect
def group_page(request, group_hash):
    if request.method == 'GET':
        user = request.user
        profile = Profile.objects.get(user=user)
        try:
            group = Group.objects.get(group_hash=group_hash, members=profile)
            members = list(group.members.all())
            members.remove(profile)
            photos = group.photos.all()
            photo_data = [] 
            for photo in photos:
                messages = Message.objects.all().filter(group=group_hash, photo=photo)
                reactions = Reaction.objects.all().filter(group=group_hash, photo=photo)
                photo_data.append({'photo': photo, 'comments': messages, 'reactions': reactions }) 
            

            data = RequestContext(request, {'profile': profile, 'group': group, 'members': members, 'photo_data': photo_data })
            return render_to_response('group.html', data)
        except Exception as e:
            print("Found an error: %s\n" %  str(e))
            return HttpResponseRedirect('/group-not-found')

@login_required
@csrf_protect
def upload_photo(request, group_hash):
    if request.method == 'POST':
        form = request.FILES
        profile = Profile.objects.get(user=request.user)
        if form['photo']:
            try:
                p = Photo.objects.create(owner=profile, image=form['photo'])
                p.save()
                g = Group.objects.get(group_hash=group_hash,members=profile)
                g.photos.add(p)
                g.save()

            except:
                return HttpResponseRedirect("/group/%s?e=1" % group_hash)
    return HttpResponseRedirect("/group/%s" % group_hash)

@login_required
@csrf_protect
def change_group_title(request):
    if request.method == 'POST':
        form = request.POST
        title = form['title']
        group_hash = form['group']
        profile = Profile.objects.get(user=request.user)
        try:
            g = Group.objects.get(creator=profile, group_hash=group_hash)
            g.group_title = title
            g.save()
            
            return request_origin(request)
        except:
            return request_origin(request, error=2)
    return request_origin(request, error=0)

@login_required
@csrf_protect
def post_comment(request):
    if request.method == 'POST':
        form = request.POST
        profile = Profile.objects.get(user=request.user)
        group = form['group']
        photo = form['photo']
        content = form['entry']

        try:
            p = Photo.objects.get(pk=photo)
            g = Group.objects.get(group_hash=group)
            m = Message.objects.create(owner=profile, photo=p, publish_date=datetime.utcnow(), group=g, content=content)
            m.save()
        
        except Exception as e:
            print("Found an error, %s\n" % e)
            return request_origin(request, error=4)

        messages = Message.objects.all().filter(photo=photo)
        data = {'profile': profile, 'messages': messages}
        html = render_to_string('post-comment.html', data)
        return JsonResponse({'html': html})

@login_required
@csrf_protect
def delete_object(request, type):
    """
    delete_object(request, type) : Deletes an object by primary key. Object type should be set by url.
    
    Obs: The form sent by the POST request must include a 'pk' parameter.
    """
    obj = {'photo': Photo, 'comment': Message}
    if request.method == 'POST':
        try:
            form = request.POST
            obj_id = form['pk']
            o = obj[type].objects.get(pk=obj_id)
            o.delete()
        
        except Exception as e:
            print("Found an error, %s\n" % e)
            return request_origin(request, error=4)
    
    return request_origin(request, error=0)

@login_required
def exemplo(request):
    user = request.user

    

def request_origin(request, error=None):
    incoming_url = request.META.get('HTTP_REFERER')
    if error:
        return HttpResponseRedirect(incoming_url + '?=' + str(error))
    else:
        return HttpResponseRedirect(incoming_url)
