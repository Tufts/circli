from rest_framework import serializers

class Serializable():
    types = {}
    def __init__(self, object_type):
        try:
            self.object = self.types[object_type][0]
            self.serializer = self.types[object_type][1]
            self.is_valid = True
        except KeyError:
            # Not a serializable.
            self.is_valid = False

    def filter(self, kwargs={}):
        self.instances = self.object.objects.filter(**kwargs)

    def serialized(self):
        try:
            serialized = self.serializer(self.instances, many=True)
        except:
            instances = self.object.objects.all()
            serialized = self.serializer(instances, many=True)

        return serialized.data



class ModelSerializerMeta(serializers.SerializerMetaclass):

    def __init__(cls, class_name, base_classes, attributes):

        super(ModelSerializerMeta, cls).__init__(class_name, base_classes, attributes)
        if cls.Meta.otype:
            Serializable.types[cls.Meta.otype] = [cls.Meta.model, cls]


class ModelSerializer(serializers.ModelSerializer):
    __metaclass__ = ModelSerializerMeta
    class Meta:
        otype = None
        model = None