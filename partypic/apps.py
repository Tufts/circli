from django.apps import AppConfig


class PartypicConfig(AppConfig):
    name = 'partypic'
