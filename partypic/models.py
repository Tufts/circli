from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
import os
import re
from random import randint
from datetime import datetime
from django.utils import timezone

from django.contrib.auth.models import User
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.conf import settings
from django.utils.encoding import smart_unicode

from partypic.functions import send_notification

from PIL import Image

# Create your models here.

class Group(models.Model):
    group_hash = models.CharField(max_length=5, primary_key=True)
    group_title = models.CharField(max_length=500, blank=True)
    creation_date = models.DateTimeField('group creation date', auto_now_add=True, blank=True)
    end_date = models.DateTimeField('group expiration date', null=True)
    creator = models.ForeignKey('Profile', related_name='creator')
    members = models.ManyToManyField('Profile', related_name='members', blank=True)
    photos = models.ManyToManyField('Photo', blank=True)
    permanent = models.BooleanField(default=False)

    def __str__(self):
        return self.group_hash if not self.group_title  else self.group_title + ' (' + self.group_hash + ')'

    def __unicode__(self): 
        unititle = self.group_hash if not self.group_title  else self.group_title + ' (' + self.group_hash + ')'
        return smart_unicode(unititle)

    def asDict(self): # Ignores photo data!
        d = {}
        d['Title'] = self.group_title
        d['Hash'] = self.group_hash
        d['Creation Date'] = self.creation_date.strftime("%Y-%m-%d %H:%M")
        d['Creator'] = self.creator.asDict()

        photos = self.photos.all()
        if photos.count() >= 1:
            d['Photo'] = self.photos.all()[0].asDict()

        d['Members'] = []
        for m in self.members.all():
            d['Members'].append(m.asDict())

        return d


class Photo(models.Model):
    owner = models.ForeignKey('Profile', on_delete=models.CASCADE)
    image = models.ImageField(upload_to = 'user-uploads/', default = 'pics/None/no-img.jpg')
    is_muted = models.BooleanField(default = False)
    submission_date = models.DateTimeField('submission date', auto_now_add=True, blank=True)
    caption = models.CharField(max_length=200, blank=True, default="")
    has_preview = models.BooleanField(default = False)
    fans = models.ManyToManyField('Profile', related_name='favorites', blank=True)

    # Possible orientations
    UP = 0
    DOWN = 180
    RIGHT = 90
    LEFT = -90

    orientation = models.IntegerField(default = UP)

    def __str__(self):
        return self.owner.user.username + " : " + self.image.name 

    def imagepath(self):
        return settings.MEDIA_URL + self.image.name

    def asDict(self):
        d ={}
        d['Id'] = self.pk
        d['Owner'] = self.owner.asDict()
        d['Url'] = settings.MEDIA_URL + self.image.name
        d['Muted'] = self.is_muted
        d['Caption'] = self.caption
        d['Timestamp'] = self.submission_date.strftime("%Y-%m-%d %H:%M:%S")
        d['Orientation'] = self.orientation
        d['Aspect'] = float(self.image.height) / float(self.image.width)

        if self.has_preview:
            d['Preview'] = re.sub(r"(\..*)", r"_preview\1", static(self.image.name))

        return d

class Avatar(Photo):
    def save(self, *args, **kwargs):
        self._meta.get_field('image').upload_to = 'user-avatars/'
        super(Avatar, self).save(*args, **kwargs)

    def asDict(self):
        d ={}
        d['Id'] = self.pk
        d['Url'] = settings.MEDIA_URL + self.image.name
        d['Timestamp'] = self.submission_date.strftime("%Y-%m-%d %H:%M")
        d['Orientation'] = self.orientation
        d['Aspect'] = float(self.image.height) / float(self.image.width)

        return d


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    real_name = models.CharField(max_length=80, blank=True)
    avatar = models.ForeignKey(Avatar, null=True)
    requests = models.ManyToManyField('Relationship', blank=True)
    is_validated = models.BooleanField(default = False)
    validation_code = models.IntegerField(default = 0)
    reset_code = models.IntegerField(default = 0)
    reset_requested = models.BooleanField(default = False)

    def __str__(self):
        return self.user.username

    def asDict(self):
        d = {}
        d['Name'] = self.user.username
        d['Real Name'] = self.real_name
        d['Is Validated'] = self.is_validated

        avatar = Avatar.objects.filter(owner=self)
        if avatar.count() >= 1:
            d['Avatar'] = avatar[0].asDict()
        

        return d

    def safe_delete(self, *args, **kwargs):
        self.user.delete()
    
    def generateValidationCode(self):
        self.validation_code = randint(10000,99999)

    def generateResetCode(self):
        self.reset_code = randint(10000,99999)
        self.reset_requested = True



class Notification(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    # Before production is published: Use timezone.now()!
    date = models.DateTimeField('notification date', auto_now_add=True, blank=True)
    content = models.CharField(max_length=200, blank=False)
    params = models.CharField(max_length=500, blank=True)

    RELATIONSHIP = 0
    PHOTO = 1
    MESSAGE = 2
    GROUP = 3
    REACTION = 4

    NOTIFICATION_TYPES = (
        (RELATIONSHIP, 'Relationship'),
        (PHOTO, 'Photo'),
        (MESSAGE, 'Message'),
        (GROUP, 'Group'),
        (REACTION, 'Reaction'),
    )
    n_type = models.IntegerField('notification type', choices=NOTIFICATION_TYPES)

    class Meta:
        unique_together = (("content", "params", "n_type"),)


class Relationship(models.Model):
    user_one = models.ForeignKey(Profile, related_name="lower_id", on_delete=models.CASCADE)
    user_two = models.ForeignKey(Profile, related_name="higher_id", on_delete=models.CASCADE)
    
    PENDING = 0
    ACCEPTED = 1
    DECLINED = 2
    BLOCKED = 3
    
    STATUS_TYPES = (
        (PENDING, 'Pending'),
        (ACCEPTED, 'Accepted'),
        (DECLINED, 'Declined'),
        (BLOCKED, 'Blocked'),
    )

    status = models.IntegerField(choices=STATUS_TYPES, default=PENDING)
    action_user = models.ForeignKey(Profile, related_name="user_with_last_action")

    def save(self, *args, **kwargs):
        if self.user_one.id >= self.user_two.id:
            print("use_one is must be lower than user_two id\n")
            return None
        else:
            super(Relationship, self).save(*args, **kwargs)

    def __str__(self):
        connector = ""
        if self.status == Relationship.PENDING:
            connector = " ... "
        elif self.status == Relationship.ACCEPTED:
            connector = " \o/ "
        else:
            connector = " _x_ "

        return self.user_one.user.username + connector + self.user_two.user.username

    def asDict(self):
        d = {}
        d['User One'] = self.user_one.user.username
        d['User Two'] = self.user_two.user.username
        d['Status'] = self.STATUS_TYPES[self.status][1]
        d['Last Action By'] = self.action_user.user.username
        return d



class Message(models.Model):
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    publish_date = models.DateTimeField('message publishing date', auto_now_add=True, blank=True)
    edit_date = models.DateTimeField('last edit date', null=True)
    group = models.ForeignKey(Group)
    content = models.TextField()

    def __str__(self):
        return self.content[0:50] + "..."

    def asDict(self):
        d = {}
        d['Id'] = self.pk
        d['Owner'] = self.owner.asDict()
        d['Refers to'] = static(self.photo.image.name)
        d['Publishing date'] = self.publish_date
        d['Last edit'] = self.edit_date if self.edit_date else self.publish_date
        d['Group'] = self.group.group_title
        d['Content'] = self.content

        return d


class Reaction(models.Model):
    LOVE = 0
    HATE = 1
    REACTION_TYPES = (
        (LOVE, 'Love'),
        (HATE, 'Hate'),
    )

    reaction = models.IntegerField(choices=REACTION_TYPES, default=LOVE)
    # Before production is published: Use timezone.now()!
    reaction_date = models.DateTimeField('message publishing date', auto_now_add=True, blank=True)
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name='reactions')
    group = models.ForeignKey(Group)

    def asDict(self):
        d = {}
        d['Id'] = self.pk
        d['Owner'] = self.owner.asDict()
        d['Reaction date'] = self.reaction_date
        d['Reaction'] = self.REACTION_TYPES[self.reaction]

        return d

    def __unicode__(self):
        return '%s: %s' % (self.owner, self.REACTION_TYPES[self.reaction][1])

class Device(models.Model):
    IOS = 0
    ANDROID = 1
    PLATFORM_TYPES = (
        (IOS, 'ios'),
        (ANDROID, 'android'),
    )

    user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=False)
    token = models.CharField(max_length=64, null=False)
    platform = models.IntegerField(choices=PLATFORM_TYPES, null=False)

    def __str__(self):
        return "%s (%s) owned by %s" % (self.token, self.PLATFORM_TYPES[self.platform][1], str(self.user))

    def notify(self, message, view, action, params):
        send_notification(self.token, message, view, action, params)
        
        print("Sent a notification to %s" % self.user)


@receiver(pre_delete, sender=Photo)
def delete_media(sender, instance, **kwargs):
    instance.image.delete(False)


#@receiver(pre_save, sender=Photo)
#def fix_path(sender, instance, **kwargs):
#    instance.image.name = re.sub("partypic/static/", "", instance.image.name)
