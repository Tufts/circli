import re
import traceback

from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import Q
from django.http import JsonResponse
from django.template import Context
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from PIL import ExifTags, Image

from oauth2_provider.views.generic import ProtectedResourceView
from partypic.functions import clean_token, send_notification
from partypic.hasher import new_hash
from partypic.models import *
from rest_framework import authentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from .serializable import Serializable
from .serializers import *

# Exceptions
from django.core.exceptions import *

# Error tracing
import linecache
import sys
import logging

DEBUG_API = False
# Get an instance of a logger
logger = logging.getLogger(__name__)

class ProtectedCsrfExemptResource(ProtectedResourceView):
    ''' ProtectedCsrfExemptResource - Wrapper view class, that exempts a ProtectedResourceView from CSRF restraints.
        Should only be used with RESTful api design.
    '''
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedCsrfExemptResource, self).dispatch(*args, **kwargs)

def new_group_hash(request):
    hash = ""
    not_found = True
    while not_found:
        hash = new_hash()
        filtered = Group.objects.all().filter(group_hash=hash)
        if len(filtered) == 0:
            not_found = False

    return JsonResponse({'Hash': hash})

def CheckUserExists(request):
    user = getProfile(request.GET['query'])
    json = {}
    if user:
        json['Exists'] = True
    
    return JsonResponse(json)

# Really should change the name. It doesn't only check, but it actually adds user to group.
class CheckGroupExists(APIView):
    # TODO: CheckGroupExists is really not a good name, as it not only checks but also adds
    # the user when found. Should rename.
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        if DEBUG_API:
            import pdb; pdb.set_trace()

        user = getProfile(request.POST['user'])
        group_hash = request.POST['group']

        g = Group.objects.filter(group_hash=group_hash)

        if g.count() > 0:
            try:
                # TODO: Fix these g[0] calls to something clearer.
                g[0].members.add(user)
                members = g[0].members.exclude(user=user.user)

                # Now that we have invited to group, let us notify the other members.
                devices = Device.objects.filter(user=members)
                for d in devices:
                    title = g[0].group_title or g[0].group_hash
                    message = "%s has joined %s" % (user.user.username, title)
                    view = "new-user-joined-group"
                    action = "reload-target,scroll-to-target"
                    params = "target=%s" % (g[0].group_hash)

                    d.notify(message, view, action, params)

                return JsonResponse({"Success": True})
            except Exception as e:
                printException()
                return JsonResponse({"Error": "Group exists, but could not add user."})
        else:
            return JsonResponse({"Error": "Group does not exist"})


class CreateGroup(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        if DEBUG_API:
            import pdb; pdb.set_trace()

        user = getProfile(request.POST['user'])
        group_hash = request.POST['hash']
        group_title = "%s's Image Group" % user.user.username

        query = Group.objects.filter(group_hash=group_hash)
        if query.count() < 1:
            group = Group.objects.create(group_hash=group_hash, group_title=group_title, creator=user)
            group.members.add(user)
            group.save()

            return JsonResponse({'Success': True})
        else:
            return JsonResponse({'Error': "Group with that hash already exists"})


class LeaveGroup(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        if DEBUG_API:
            import pdb; pdb.set_trace()

        user = getProfile(request.POST['user'])
        group_hash = request.POST['group']

        try:
            group = Group.objects.get(group_hash=group_hash)
            group.members.remove(user)

            if group.members.count() < 1:
                # All members have left! Group will be deleted, including photos.
                group.delete()
            else: # Else, notify other users.
                members = group.members.all()
                devices = Device.objects.filter(user=members)
                for d in devices:
                    title = group.group_title or group.group_hash
                    message = "%s has left %s" % (user.user.username, title)
                    view = "user-left-group"
                    action = "reload-target,scroll-to-target"
                    params = "target=%s" % (group.group_hash)

                    d.notify(message, view, action, params)
        except:
            printException()
            return JsonResponse({'Error': "Server error"})

        return JsonResponse({'Success': True})


class CreateUser(APIView):
    permission_classes = (AllowAny,)
    def post(self, request, *args, **kwargs):
        if DEBUG_API:
            import pdb; pdb.set_trace()

        username = request.POST['username']
        real_name = request.POST['real_name']
        password = request.POST['password']
        email = request.POST['email']

        # Email needs to be unique.
        if User.objects.filter(email=email).count() >= 1:
            print("CreateUser - %s is not a unique email." % email)
            return JsonResponse({'Error': "Could not create new account. This email has already been used."})

        # Attempt to create a new user, with details.
        json = {}
        try:
            # TODO: Do this with a form?
            user = User.objects.create_user(username = username, password = password, email = email)
            profile = Profile.objects.create(user=user, real_name=real_name)
            profile.generateValidationCode()
            profile.save()
            json = {'Profile': profile.asDict()}

        except Exception as e:
            print("CreateUser - %s" % str(e))
            json = {'Error': "Could not create user profile."}

        try:
            host =  settings.EMAIL_HOST_USER
            name = profile.user.username
            if profile.real_name != "":
                name= profile.real_name

            message = get_template('email.txt').render (
                Context({
                    'name': name,
                    'code': profile.validation_code,
                })
            )
            html = get_template('email.html').render (
                Context({
                    'name': name,
                    'code': profile.validation_code,
                })
            )
            send_mail('Welcome to Circli', message, host, [email], html_message=html, fail_silently=False)
        
        except Exception as e:
            print("CreateUser(send_mail) - %s" % str(e))
            user.delete()
            return JsonResponse({'Error': "Could not create user profile."})
        
        return JsonResponse(json)

class ValidateUser(APIView):
    
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        profile = getProfile(request.POST['user'])
        submitted_code = int(request.POST['code'])
        
        if profile.validation_code == submitted_code:
            # User has input the correct code.
            profile.is_validated = True
            profile.save()

        return JsonResponse({'Profile': profile.asDict()})

class ResendConfirmationEmail(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, format=None):
        profile = getProfile(request.GET['user'])
        profile.generateValidationCode()

        try:
            host =  settings.EMAIL_HOST_USER
            email = profile.user.email
            name = profile.user.username
            if profile.real_name != "":
                name= profile.real_name
            
            message = get_template('email.txt').render (
                Context({
                    'name': name,
                    'code': profile.validation_code,
                })
            )
            html = get_template('email.html').render (
                Context({
                    'name': name,
                    'code': profile.validation_code,
                })
            )
            send_mail('Welcome to Circli', message, host, [email], html_message=html, fail_silently=False)
        
        except Exception as e:
            print("ResendConfirmationEmail(send_mail) - %s" % str(e))
            return JsonResponse({'Error': "Server error - Could not send confirmation email."})

        profile.save()
        return JsonResponse({'Profile': profile.asDict()})


class RequestPasswordReset(APIView):
    '''
    RequestPasswordReset - This call is used to generate a password reset code,
    that can then be used after to effectively change the password of an account.
    '''
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        email = request.GET['email']

        try:
            user = User.objects.get(email=email)

        except User.DoesNotExist:
            # This email has no registered account.
            return JsonResponse({'Error': "No Circli account has been registered with this email"})
        except MultipleObjectsReturned:
            # This email has multiple accounts. Cannot choose.
            return JsonResponse({'Error': "This email has multiple accounts attached. Please contact Circli to solve this issue."})

        try:
            host = settings.EMAIL_HOST_USER
            name = user.username
            profile = user.profile

            # Initiate reset request.
            profile.generateResetCode()

            if profile.real_name != "":
                name = profile.real_name
            
            message = get_template('password_reset.txt').render (
                Context({
                    'name': name,
                    'code': profile.reset_code,
                })
            )
            html = get_template('password_reset.html').render (
                Context({
                    'name': name,
                    'code': profile.reset_code,
                })
            )
            # TODO: Make this asynchronous.
            send_mail('Reset your Circli Account Password', message, host, [email], html_message=html, fail_silently=False)
            profile.save()
        except:
            # If ANYTHING goes wrong, we need to invalidate the reset rode.
            profile.reset_requested = False
            profile.save()
            return JsonResponse({'Error': "Could not send password reset code."})

        return JsonResponse({'Success': True})



class ResetPassword(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        try:
            email = request.POST['email']
            password = request.POST['password']
            reset_code = int(request.POST['reset_code'])
        except:
            printException()
            return JsonResponse({'Error': "Incorrect use of api call."})

        try:
            user = User.objects.get(email=email)

        except User.DoesNotExist:
            # This email has no registered account.
            return JsonResponse({'Error': "No Circli account has been registered with this email"})
        except MultipleObjectsReturned:
            # This email has multiple accounts. Cannot choose.
            return JsonResponse({'Error': "This email has multiple accounts attached. Please contact Circli to solve this issue."})

        # Now that we have the correct email, let's check for reset code. If correct, change password.

        p = user.profile
        if not p.reset_requested or reset_code != p.reset_code:
            return JsonResponse({'Error': "Incorrect or expired password reset code"})

        try:
            user.set_password(password)
            p.reset_requested = False
            p.save()
            user.save()
        except:
            printException()
            return JsonResponse({'Error': "Could not update password"})

        return JsonResponse({'Success': True})



class GetGroupContent(ProtectedCsrfExemptResource):
    def get(self, request, *args, **kwargs):
        hash = ""
        try:
            hash = request.GET['hash']

        except Exception as e:
            print("2 - Found an error, %s", str(e))
            return JsonResponse({'Error': "Could not find the hash GET attribute."})

        if hash:
            json = []
            try:
                group = Group.objects.get(group_hash=hash)
                photos = group.photos.all().order_by('-submission_date')

                for p in photos:
                    messages = Message.objects.all().filter(group=group, photo=p)
                    reactions = Reaction.objects.filter(photo=p, group=group)
                    m_json = [{'Message': m.asDict()} for m in messages]   
                    r_json = [{'Reaction': r.asDict()} for r in reactions]           
                    json.append({'Photo': p.asDict(), 'Messages': m_json, 'Reactions': r_json})

                return JsonResponse({'Photos': json})
            except Exception as e:
                print("3 - Found an error, %s", str(e))
                return JsonResponse({'Error': "No such group."})

        else:
            return JsonResponse({'Error': "Could not complete request. Try again later."})

class ListGroupsByUser(ProtectedCsrfExemptResource):
    def get(self, request, *args, **kwargs):
        json = []
        try:
            if DEBUG_API:
                import pdb; pdb.set_trace()

            username = request.GET['user']
            user = User.objects.get(username=username)
            profile = Profile.objects.get(user=user)
            groups = Group.objects.all().filter(members=profile)
            for g in groups:
                json.append({'Group': g.asDict()})
            return JsonResponse({'Groups': json})
        
        except Exception as e:
            printException()
            print("4 - Found an error: %s\n" %  str(e))

        return JsonResponse({'Error': 'No groups'})


class RequestRelationship(ProtectedCsrfExemptResource):
    def post(self, request, *args, **kwargs):
        try:
            user = getProfile(request.POST['user'])
            friend = getProfile(request.POST['requested-user'])
            if (user == None) or (friend == None):
                return JsonResponse({'Error': "Bad request. Invalid users."})

            # First check if the relationship already exists.
            check = Relationship.objects.all().filter(user_one=user, user_two=friend) | Relationship.objects.all().filter(user_one=friend, user_two=user)
            if len(check) > 0:
                return JsonResponse({'Error': "Bad request. Relationship already exists."})
            
            r = None
            if user.id < friend.id:
                r = Relationship.objects.create(user_one=user, user_two=friend, status=Relationship.PENDING, action_user=user)
            elif user.id > friend.id:
                r = Relationship.objects.create(user_one=friend, user_two=user, status=Relationship.PENDING, action_user=user)
            else:
                return JsonResponse({'Error': "Bad request. Attempted to request relationship to self."})
            
            # Send request notification to user.
            devices = Device.objects.filter(user=friend)
            for d in devices:
                message = "You have received a friend request from %s" % (user.user.username)
                view = "request-friendship"
                action = ""
                params = ""

                d.notify(message, view, action, params)
            return JsonResponse({'Message': "Relationship request sent."})
            
        except Exception as e:
            printException()
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})


class AcceptRelationship(ProtectedCsrfExemptResource):
    def post(self, request, *args, **kwargs):
        try:
            user = getProfile(request.POST['user'])
            friend = getProfile(request.POST['accepted-user'])

            if (user == None) or (friend == None):
                return JsonResponse({'Error': "Bad request. Invalid users."})

            # Check if relationship exists
            r = None
            if user.user.id <= friend.user.id:
                r = Relationship.objects.get(user_one=user, user_two=friend)
            else:
                r = Relationship.objects.get(user_one=friend, user_two=user)

            if r != None and r.status == Relationship.PENDING:
                r.status = Relationship.ACCEPTED
                r.action_user = user
                r.save()
            else:
                return JsonResponse({'Error': "Bad request. Pending relationship does not exist."})

            # Need to find who is the requester, and the requested.
            # Send request notification to original user that sent the request.
            devices = Device.objects.filter(user=friend)
            for d in devices:
                message = "%s has accepted your friend request" % (user.user.username)
                view = "accept-friendship"
                action = ""
                params = ""

                d.notify(message, view, action, params)
                
            return JsonResponse({'Message': "Friend request accepted."})
        except Exception as e:
            print(str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})

class GetRelationships(ProtectedCsrfExemptResource):
    def get(self, request, *args, **kwargs):
        json = []
        try:
            user = getProfile(request.GET['user'])
            status = getStatusCode(request.GET['status'])

            # Find relationships that include the user.
            relationships = Relationship.objects.filter(Q(user_one=user) | Q(user_two=user))

            # Filter by status
            relationships = relationships.filter(status=status)

            # Special case: pending ->  Get only incoming requests (not outgoing)
            if status == Relationship.PENDING:
                relationships = relationships.filter(~Q(action_user=user))

            for r in relationships:
                json.append({'Relationship': r.asDict()})

            return JsonResponse({'Relationships': json})

        except Exception as e:
            print("6 - Found an error %s\n", str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})


class SearchUsers(ProtectedCsrfExemptResource):
    ALL = 0
    FRIENDS = 1
    NONFRIENDS = 2

    def get(self, request, *args, **kwargs):
        try:
            if DEBUG_API:
                import pdb; pdb.set_trace()

            owner = getProfile(request.GET['user'])
            pattern = request.GET['query']
            category = int(request.GET['query-type'])

            users_prefilter = User.objects.filter(Q(username__icontains=pattern) & ~Q(username=owner.user.username))
        
            users = []
            
            #BEGIN filtering by category
            if category == SearchUsers.NONFRIENDS:
                for u in users_prefilter:
                    p = Profile.objects.get(user=u)
                    r = None
                    if p.user.id <= owner.user.id:
                        r = Relationship.objects.filter(user_one=p, user_two=owner)
                    else:
                        r = Relationship.objects.filter(user_one=owner, user_two=p)

                    if len(r) == 0:
                        users.append(u)

            elif category == SearchUsers.ALL:
                users = users_prefilter
            

            #END

            json = [{'Profile': Profile.objects.get(user=user).asDict()} for user in users]
            return JsonResponse({'Profiles': json})

        except Exception as e:
            print("7 - Found an error %s\n", str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})


class GetUserDetails(ProtectedCsrfExemptResource):
    def get(self, request, *args, **kwargs):
        try:
            user = getProfile(request.GET['user'])
            return JsonResponse({'User': user.asDict()})
        
        except Exception as e:
            print("8 - Found an error %s\n" % str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})

class UploadImage(ProtectedCsrfExemptResource):
    # TODO: Fix potential object and string conflicts.
    def post(self, request, *args, **kwargs):
        try:
            user = getProfile(request.POST['user'])
            image = request.FILES['Image']
            group = request.POST['group']
            rotation = int(request.POST['rotate'])
            caption = request.POST['caption']

            p = Photo.objects.create(owner=user, image=image, orientation=rotation)
            p.caption = caption

            try:
                #preview = re.sub(r"(\..*)", r"_preview\1", p.image.name)
                p_image = Image.open(p.image.path)
                p_image = p_image.rotate(rotation)

                if DEBUG_API:
                    import pdb; pdb.set_trace()
                 
                #resize_ratio = 600/p_image.size[0]
                #new_width = int(resize_ratio*p_image.size[0])
                #new_height = int(resize_ratio*p_image.size[1])
                #new_dims = [new_width, new_height]

                #p_image = p_image.resize(new_dims, Image.ANTIALIAS)
                #p_image.save(preview, quality=95)
                #p.has_preview = True

                g = Group.objects.get(group_hash=group,members=user)
                g.photos.add(p)

                g.save()
                p.save()

                # Now that we have uploaded the photo, let us notify all members of the group.
                members = g.members.exclude(user=user.user)
                devices = Device.objects.filter(user=members)
                for d in devices:
                    title = g.group_title or g.group_hash
                    message = "%s has posted a new photo in %s" % (user.user.username, title)
                    view = "new-photo-shared-in-group"
                    action = "reload-target,scroll-to-target"
                    params = "group=%s,target=%d" % (g.group_hash,p.id)

                    d.notify(message, view, action, params)

            except Exception as e:
                print("UploadImage(rotation) - %s" % str(e))
                printException()

            return JsonResponse({'Success': True})

        except Exception as e:
            print("9 - Found an error %s\n" % str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})


class UploadAvatar(ProtectedCsrfExemptResource):
    def post(self, request, *args, **kwargs):
        try:
            user = getProfile(request.POST['user'])
            image = request.FILES['Image']
            rotation = int(request.POST['rotate'])

            # First, get any old avatars, to delete later.
            old = Avatar.objects.filter(owner=user)

            # Now create a new one.
            p = Avatar.objects.create(owner=user, image=image, orientation=rotation)

            try:
                #preview = re.sub(r"(\..*)", r"_preview\1", p.image.name)
                p_image = Image.open(p.image.path)
                p_image = p_image.rotate(rotation)

            except Exception as e:
                print("UploadImage(rotation) - %s" % str(e))
                printException()
            
            # Now that everything has worked out perfectly, delete old.
            for old_avatar in old.exclude(pk=p.pk):
                old_avatar.delete()

            return JsonResponse({'Success': True})

        except Exception as e:
            printException()
            print("UploadAvatar - %s" % str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})


class LikePhoto(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        profile = getProfile(request.POST['user'])
        photo_id = int(request.POST['photo-id'])
        group_hash = request.POST['group']

        photo = Photo.objects.get(pk=photo_id)
        group = Group.objects.get(group_hash=group_hash)

        query = Reaction.objects.filter(reaction=Reaction.LOVE, owner=profile, photo=photo, group=group)
        if query.count() < 1:
            # Like does not yet exist, register new like.
            like = Reaction.objects.create(reaction=Reaction.LOVE, owner=profile, photo=photo, group=group)


            # Now that we have liked the photo, let us notify the owner (if possible).
            try:
                content = "%s likes the image you posted" % profile.user.username
                view = "photo-liked"
                action = "reload-target,scroll-to-target"
                params = "group=%s,target=%d" % (group.group_hash,photo_id)

                try:
                    n = Notification(user=photo.owner, content=content, params=params, n_type=Notification.REACTION)
                    n.save()
                except:
                    printException()

                devices = Device.objects.filter(user=photo.owner).exclude(user=profile)
                for d in devices:
                    d.notify(content, view, action, params)

            except:
                printException()

            # Let's also notify interested (subscribed) users.
            try:
                fans = photo.fans.exclude(id=profile.id).exclude(id=photo.owner.id)
                devices = Device.objects.filter(user=fans)
                for d in devices:
                    message = "%s has liked the photo %s posted" % (profile.user.username, photo.owner.user.username)
                    view = "photo-liked"
                    action = "reload-target,scroll-to-target"
                    params = "group=%s,target=%s" % (group.group_hash, photo_id)

                    d.notify(message, view, action, params)
            except Exception as e:
                printException()
                print("LikePhoto (notification 2) - %s\n" % str(e))

            return JsonResponse({'Reaction': like.asDict()})

        else:
            # Photo already liked, unlike photo.
            query[0].delete()
            return JsonResponse({'Deleted': True})

class SendMessage(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        try:
            if DEBUG_API:
                import pdb; pdb.set_trace()

            profile = getProfile(request.POST['user'])
            photo_id = int(request.POST['photo'])
            group_hash = request.POST['group']
            content = request.POST['content']

            photo = Photo.objects.get(pk=photo_id)
            group = Group.objects.get(group_hash=group_hash)
            message = Message.objects.create(owner=profile, photo=photo, group=group, content=content)
            message.save()
            message_id = message.id

            # Now that we have left a message, let us notify the owner (if possible).
            try:
                devices = Device.objects.filter(user=photo.owner).exclude(user=profile)
                for d in devices:
                    message = "%s has commented on an image you posted" % profile.user.username
                    view = "new-comment"
                    action = "reload-target,scroll-to-target"
                    params = "group=%s,photo=%s,target=%s" % (group.group_hash, photo_id, message_id)

                    d.notify(message, view, action, params)
            except Exception as e:
                printException()
                print("SendMessage(notification) - %s\n" % str(e))

            # Let's also notify interested (subscribed) users.
            try:
                fans = photo.fans.exclude(id=profile.id).exclude(id=photo.owner.id)
                devices = Device.objects.filter(user=fans)
                for d in devices:
                    message = "%s has commented on the photo %s has posted" % (profile.user.username, photo.owner.user.username)
                    view = "new-comment"
                    action = "reload-target,scroll-to-target"
                    params = "group=%s,photo=%s,target=%s" % (group.group_hash, photo_id, message_id)

                    d.notify(message, view, action, params)
            except Exception as e:
                printException()
                print("SendMessage(notification 2) - %s\n" % str(e))


            # Now that the devices have been notified of this event,
            # let us make sure that the current user has access to future
            # notifications as well.
            # First check if he's not already subscribed.
            if not profile.favorites.filter(id=photo_id).exists():
                # If so, subscribe.
                photo.fans.add(profile)

            return JsonResponse({'Success': True})

        except Exception as e:
            print("SendMessage - %s\n" % str(e))
            return JsonResponse({'Error': "There was a problem with the server. Try again later."})   

class InviteToGroup(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        try:
            logger.debug("Got here")
            invited_by = getProfile(request.POST['invited-by'])
            invitee = getProfile(request.POST['invitee'])
            group_hash = request.POST['group']

            group = Group.objects.get(group_hash=group_hash)
            group.members.add(invitee)
            members = group.members.exclude(user=invited_by.user).exclude(user=invitee.user)
        except:
            printException()
            return JsonResponse({'Error': "Could not add user to group"})

        # Now that we have invited to group, let us notify the other members.
        try:
            print("Got here")
            devices = Device.objects.filter(user=members)
            for d in devices:
                title = group.group_title or group.group_hash
                message = "%s has joined %s" % (invitee.user.username, title)
                view = "new-user-joined-group"
                action = "reload-target,scroll-to-target"
                params = "target=%s" % (group.group_hash)

                d.notify(message, view, action, params)
        except:
            printException()
            return JsonResponse({'Success': "User added to group, but could not notify existing members"})

        
        # Send to invitee.
        try:
            devices = Device.objects.filter(user=invitee)
            for d in devices:
                title = group.group_title or group.group_hash
                message = "%s has added you to %s" % (invited_by.user.username, title)
                view = "added-to-group"
                action = "reload-target,scroll-to-target"
                params = "target=%s" % (group.group_hash)

                d.notify(message, view, action, params)
        except:
            printException()
            return JsonResponse({'Success': "User invited to group, but could not notify the user itself"})
        
        return JsonResponse({'Success': True})

class RegisterDevice(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        user = getProfile(request.POST['user'])
        token = request.POST['token']
        uid = request.POST['uid']
        platform = request.POST['platform']

        try:
            cleanedtoken = clean_token(token)
            pc = getPlatformCode(platform)
            if pc == Device.IOS or pc == Device.ANDROID:
                # First check if device already has a registered token. If so, simply update.
                query = Device.objects.filter(user=user)
                if query.count() > 0:
                    d = query[0]
                    d.user = user
                    d.token = cleanedtoken
                    d.platform = pc
                    d.save()
                else: # Create new Device.
                    # TODO: Add back UID support, so that we can update device token!
                    device = Device.objects.create(user=user, token=cleanedtoken, platform=pc)
                    device.save()
            else:
                return JsonResponse({"Error": "Did not expect mobile platform - %s" % platform})

        except Exception as e:
            printException()
            return JsonResponse({"Error": "Token already added"})
        
        return JsonResponse({"Success": True})


class UpdateGroupName(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def post(self, request, format=None):
        user = getProfile(request.POST['user'])
        group_hash = request.POST['group']
        new_title = request.POST['title']

        try:
            group = Group.objects.get(group_hash=group_hash)
            group.group_title = new_title
            group.save()

            return JsonResponse({'Success': True})

        except Exception as e:
            print("UpdateGroupName - %s\n" % str(e))
            return JsonResponse({'Error': 'Could not change group title.'})


class GetObject(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    #permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        object_type = request.GET['type']
        object_id = request.GET['id']

        serializable = Serializable(object_type)

        if serializable.is_valid:
            # FIXME: What happens when id does not exist?
            serializable.filter({'id':object_id})
            json = serializable.serialized()

            return JsonResponse(json, safe=False)

        else:
            return JsonResponse({'Error': "No such %s with id=%s" % (object_type, object_id)})


## Auxiliary Functions -  These are not API views!
def getProfile(name):
    profile = None
    try:
        user = User.objects.get(username=name)
        profile = user.profile
    except Exception as e:
        return None

    return profile


def getStatusCode(status):
    s = status.lower()
    if s == 'pending':
        return Relationship.PENDING
    elif s == 'accepted':
        return Relationship.ACCEPTED
    elif s == 'blocked':
        return Relationship.BLOCKED
    elif s == 'declined':
        return Relationship.DECLINED
    else:
        return -1

def getPlatformCode(platform):
    p = platform.lower()
    if p == 'ios':
        return Device.IOS
    elif p == 'android':
        return Device.ANDROID
    else:
        return -1

def printException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)


