import os
import re

def send_notification(device, message, view, action, params):
    cmd = "php circli-pusher.php '%s' '%s' '%s' '%s' '%s' >> push.log" % (device, message, view, action, params)
    os.system(cmd)
    return

def clean_token(rawtoken):
    match = re.search('<([a-f 0-9]+)>', rawtoken)

    if match:
        token = re.sub('\s+', '', match.group(1))
        return token
    else:
        return 0 # Token is not in expected form.