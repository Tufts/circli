# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('uid', models.CharField(max_length=64, serialize=False, primary_key=True)),
                ('token', models.CharField(max_length=64)),
                ('platform', models.IntegerField(choices=[(1, b'ios'), (2, b'android')])),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('group_hash', models.CharField(max_length=5, serialize=False, primary_key=True)),
                ('group_title', models.CharField(max_length=500, blank=True)),
                ('creation_date', models.DateTimeField(default=datetime.datetime(2017, 5, 30, 17, 6, 39, 854496), verbose_name=b'group creation date')),
                ('permanent', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('publish_date', models.DateTimeField(default=datetime.datetime(2017, 5, 30, 17, 6, 39, 860567), verbose_name=b'message publishing date')),
                ('edit_date', models.DateTimeField(null=True, verbose_name=b'last edit date')),
                ('content', models.TextField()),
                ('group', models.ForeignKey(to='partypic.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=datetime.datetime(2017, 5, 30, 17, 6, 39, 858965), verbose_name=b'notification date')),
                ('content', models.CharField(max_length=200)),
                ('params', models.CharField(max_length=500, blank=True)),
                ('n_type', models.IntegerField(verbose_name=b'notification type', choices=[(0, b'Relationship'), (1, b'Photo'), (2, b'Message'), (3, b'Group'), (4, b'Reaction')])),
            ],
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default=b'pics/None/no-img.jpg', upload_to=b'user-uploads/')),
                ('is_muted', models.BooleanField(default=False)),
                ('submission_date', models.DateTimeField(default=datetime.datetime(2017, 5, 30, 17, 6, 39, 856715), verbose_name=b'submission date')),
                ('caption', models.CharField(default=b'', max_length=200, blank=True)),
                ('has_preview', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('real_name', models.CharField(max_length=80, blank=True)),
                ('is_validated', models.BooleanField(default=False)),
                ('validation_code', models.IntegerField(default=0)),
                ('avatar', models.ForeignKey(to='partypic.Photo', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Reaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reaction', models.IntegerField(default=1, choices=[(1, b'Love'), (2, b'Hate')])),
                ('reaction_date', models.DateTimeField(default=datetime.datetime(2017, 5, 30, 17, 6, 39, 861333), verbose_name=b'message publishing date')),
                ('group', models.ForeignKey(to='partypic.Group')),
                ('owner', models.ForeignKey(to='partypic.Profile')),
                ('photo', models.ForeignKey(to='partypic.Photo')),
            ],
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.IntegerField(default=0, choices=[(0, b'Pending'), (1, b'Accepted'), (2, b'Declined'), (3, b'Blocked')])),
                ('action_user', models.ForeignKey(related_name='user_with_last_action', to='partypic.Profile')),
                ('user_one', models.ForeignKey(related_name='lower_id', to='partypic.Profile')),
                ('user_two', models.ForeignKey(related_name='higher_id', to='partypic.Profile')),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='requests',
            field=models.ManyToManyField(to='partypic.Relationship', blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='photo',
            name='owner',
            field=models.ForeignKey(to='partypic.Profile'),
        ),
        migrations.AddField(
            model_name='notification',
            name='user',
            field=models.ForeignKey(to='partypic.Profile'),
        ),
        migrations.AddField(
            model_name='message',
            name='owner',
            field=models.ForeignKey(to='partypic.Profile'),
        ),
        migrations.AddField(
            model_name='message',
            name='photo',
            field=models.ForeignKey(to='partypic.Photo'),
        ),
        migrations.AddField(
            model_name='group',
            name='creator',
            field=models.ForeignKey(related_name='creator', to='partypic.Profile'),
        ),
        migrations.AddField(
            model_name='group',
            name='members',
            field=models.ManyToManyField(related_name='members', to='partypic.Profile', blank=True),
        ),
        migrations.AddField(
            model_name='group',
            name='photos',
            field=models.ManyToManyField(to='partypic.Photo', blank=True),
        ),
        migrations.AddField(
            model_name='device',
            name='user',
            field=models.ForeignKey(to='partypic.Profile'),
        ),
    ]
