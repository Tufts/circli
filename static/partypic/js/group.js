// using jQuery
function deleteMenu(element) {
    $(element).find('.dropdown-content').toggle();
}
// Function below hides the menu.
window.onclick = function(event) {
  if (!event.target.matches('.photo-delete')) {
      $('.dropdown-content').hide();
  }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // The methods below do not require csrf protection.
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

document.getElementById("photo_uploader").onchange = function() {
    document.getElementById("upload_form").submit();
}
document.getElementById("group_title").onblur = function() {
    var group = window.location.pathname.match(/\/group\/(.*)\//)[1];
    $.ajax({
        type: 'post',
        url: '/change-group-title/',
        data: 'title=' +$('#group_title').text().trim()+ '&group=' +group
    });
}
$('.comment-entry').keydown(function(event) {
    if (event.keyCode == 13) { // 13 = Enter
        var content = $(this).val();

        // Now that we have the content, remove the text from input, so user can know that it has been entered.
        $(this).val("");
        $(this).blur();

        var id = $(this).closest("div").find("input[name='photo']").val();
        var group = $(this).closest("div").find("input[name='group']").val();

        // Find the correct element to insert data.
        var table = $(this).closest('.photo').find('table')
        $.ajax({
            type: 'post',
            url: '/post-comment/',
            data: {group: group, photo: id, entry: content},
            success: function(data) {
                $(table).html(data["html"]);
                $(table).find('.delete-comment').click(function() {
                    deleteComment(this);
                })
            }
        });
    }
});
$('.delete-comment').click(function() {
    deleteComment(this);
});

function deleteComment(element) {
    if(window.confirm("Are you sure you want to delete this message? You will not be able to undo this action.")) {
        var id = $(element).parent('form').find('input[name="pk"]')[0].value;
        var commentRow = $(element).closest('tr').remove();
        $.ajax({
            type: 'post',
            url: '/delete-comment/',
            data: 'pk=' +id,
        });
    }
}
function deletePhoto(element) {
    if (window.confirm("Are you sure you want to delete this photo? You will not be able to undo this action.")) {
        $(element).closest('.photo').remove();
        var id = $(element).find('input').val();
        $.ajax({
            type: 'post',
            url: '/delete-photo/',
            data: 'pk=' +id,
        });
    }
}