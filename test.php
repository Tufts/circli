<?php


$deviceToken = $argv[1];
$message = $argv[2];
$view = $argv[3];
$action = $argv[4];
$params = explode(",", $argv[5]);

$parameters = array();
foreach ($params as $parameter) {
    $aux = explode("=", $parameter);
    $parameters[$aux[0]] = $aux[1];
}
print_r($parameters);

$body['aps'] = array(
  'alert' => $message,
  'sound' => 'default',
  'badge' => 1,
  'view' => $view,
  'action' => $action,
  'params' => $parameters,
  );

// Encode the payload as JSON
$payload = json_encode($body);
print($payload);

?>
